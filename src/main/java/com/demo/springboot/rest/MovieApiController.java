package com.demo.springboot.rest;

import com.demo.springboot.MovieService;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/api")
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);


    private final MovieService movieService;

    @Autowired
    public MovieApiController(MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> getMovieService() {
        return ResponseEntity.ok().body(new MovieListDto(movieService.read()));    // = new ResponseEntity<>(movies, HttpStatus.OK);
    }


    @PutMapping("/movies/{id}")
    public ResponseEntity<Void> updateMovie(@RequestBody MovieDto MovieDto,@PathVariable String id) {

        if(movieService.update(MovieDto,Integer.parseInt(id))) {
            return ResponseEntity.ok().build();
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/movies/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable String id) {
        if(movieService.delete(Integer.parseInt(id))) {
            return ResponseEntity.ok().build();
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/movies")
    public ResponseEntity<Void> createMovie(@RequestBody MovieDto MovieDto) throws URISyntaxException {

        if(movieService.create(MovieDto)) {
            return ResponseEntity.created(new URI("/movies/" + MovieDto.getMovieId())).build();
        }
        else {
            return ResponseEntity.badRequest().build();
        }
    }
}
