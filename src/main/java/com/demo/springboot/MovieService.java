package com.demo.springboot;

import com.demo.springboot.dto.MovieDto;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

@Service
public class MovieService {

    private List<MovieDto> moviesList = new ArrayList<>();
    public MovieService(){
        moviesList.add(new MovieDto(1,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
    }
    public List<MovieDto> read(){
        List <MovieDto> reversedList = new ArrayList<>(moviesList);
        Collections.reverse(reversedList);
        return reversedList;
    }
    public boolean create(MovieDto movieDto){
        Integer newId = moviesList.get(moviesList.size()-1).getMovieId()+1;
        if(Stream.of(movieDto.getTitle(), movieDto.getYear(),movieDto.getImage()).anyMatch(StringUtils::isEmpty)){
            return false;
        }
        movieDto.setMovieId(newId);
        moviesList.add(movieDto);
        return true;
    }
    public boolean delete(Integer movieId) {
        MovieDto m = moviesList.stream().filter(el -> movieId.equals(el.getMovieId())).findFirst().orElse(null);
        if (m != null) {
            moviesList.remove(m);
            return true;
        } else {
            return false;
        }
    }
    public boolean update(MovieDto movieDto,Integer movieId) {
        MovieDto m = moviesList.stream().filter(el -> movieId.equals(el.getMovieId())).findFirst().orElse(null);
        if (m != null) {
            if(Stream.of(movieDto.getTitle(), movieDto.getYear(),movieDto.getImage()).anyMatch(StringUtils::isEmpty)){
                return false;
            }
            m.setTitle(movieDto.getTitle());
            m.setYear(movieDto.getYear());
            m.setImage(movieDto.getImage());
            return true;
        } else {
            return false;
        }
    }
}
